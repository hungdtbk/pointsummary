package com.altplus.hungdt.pointsummary.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.altplus.hungdt.pointsummary.R;

/**
 * Created by Nguyen Dang Hung on 1/3/2017.
 */

public class DialogUtils {
    public static void showConfirmDialog(Context context, int stringID, DialogInterface.OnClickListener okListener) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context)
                .setMessage(context.getString(stringID))
                .setPositiveButton(R.string.ok, okListener)
                .setNegativeButton(R.string.cancel, null);
        dialog.show();
    }
}
