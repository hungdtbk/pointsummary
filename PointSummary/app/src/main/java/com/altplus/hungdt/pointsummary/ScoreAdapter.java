package com.altplus.hungdt.pointsummary;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.LinkedList;

/**
 * Created by Nguyen Dang Hung on 10/28/2016.
 */
public class ScoreAdapter extends RecyclerView.Adapter<ScoreAdapter.ScoreViewHolder> {
    private int mNumberPlayers = 5;
    LinkedList<Item> mItems = new LinkedList<>();

    private Context mContext;

    public interface OnItemLongClickListener{
        void onItemLongClick();
    }
    private OnItemLongClickListener mOnItemLongClickListener;

    public ScoreAdapter(LinkedList<Item> items) {
        mItems = items;
    }

    @Override
    public ScoreViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_score, parent, false);
        return new ScoreViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ScoreViewHolder holder, int position) {
        for (int i = 0; i < AppConstants.MAX_NUMBER_PLAYER; i++) {
            if (i >= mNumberPlayers) {
                holder.mTvScores[i].setVisibility(View.GONE);
            } else {
                holder.mTvScores[i].setText(Integer.toString(mItems.get(position).getNumbers().get(i)));
            }
        }
        if (position == mItems.size() - 1) {
            holder.mLlContainer.setBackgroundColor(mContext.getResources().getColor(R.color.yellow));
        } else {
            holder.mLlContainer.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.bg_item_score_selector_white));
        }
        holder.mItemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                mOnItemLongClickListener.onItemLongClick();
                ((MainActivity)mContext).openContextMenu(view);
                return false;
            }
        });

    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void updateRowsVisibility(int n) {
        mNumberPlayers = n;
        notifyDataSetChanged();
    }

    public void setOnItemLongClickListener(OnItemLongClickListener mOnItemLongClickListener) {
        this.mOnItemLongClickListener = mOnItemLongClickListener;
    }

    public class ScoreViewHolder extends RecyclerView.ViewHolder {
        private final int[] tvIds = {R.id.tv_score_1, R.id.tv_score_2, R.id.tv_score_3, R.id.tv_score_4, R.id.tv_score_5, R.id.tv_score_6};
        TextView[] mTvScores;
        LinearLayout mLlContainer;
        View mItemView;

        public ScoreViewHolder(View itemView) {
            super(itemView);
            mTvScores = new TextView[AppConstants.MAX_NUMBER_PLAYER];
            for (int i = 0; i < AppConstants.MAX_NUMBER_PLAYER; i++) {
                mTvScores[i] = (TextView) itemView.findViewById(tvIds[i]);
            }
            mLlContainer = (LinearLayout) itemView.findViewById(R.id.ll_item_container);
            mItemView = itemView;
        }
    }
}
