package com.altplus.hungdt.pointsummary;

import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.altplus.hungdt.pointsummary.customview.MyAutoCompleteTextView;
import com.altplus.hungdt.pointsummary.utils.DialogUtils;

import java.util.ArrayList;
import java.util.LinkedList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private int mNumberPlayers = 4;

    MyAutoCompleteTextView[] mEdtNames;
    ImageButton[] mBtnUp;
    ImageButton[] mBtnDown;

    RecyclerView mRcvListScore;

    TextView[] mTv;

    LinearLayout[] mLlNps;

    ImageView mImgAdd;
    ScrollView mScrollView;


    TextView mTvMatch;
    private ScoreAdapter mAdapter;
    private LinkedList<Item> mItems;
    private int[] mScores;
    private int[] edtIds = {R.id.edt_name_1, R.id.edt_name_2, R.id.edt_name_3, R.id.edt_name_4, R.id.edt_name_5, R.id.edt_name_6};
    private int[] npIds = {R.id.tv1, R.id.tv2, R.id.tv3, R.id.tv4, R.id.tv5, R.id.tv6};
    private int[] btnUp = {R.id.up1, R.id.up2, R.id.up3, R.id.up4, R.id.up5, R.id.up6};
    private int[] btnDown = {R.id.down1, R.id.down2, R.id.down3, R.id.down4, R.id.down5, R.id.down6};
    private int[] llNps = {R.id.ll_np_1, R.id.ll_np_2, R.id.ll_np_3, R.id.ll_np_4, R.id.ll_np_5, R.id.ll_np_6};
    private int[] mMenuSetPlayers = {R.id.menu_set_players_2, R.id.menu_set_players_3, R.id.menu_set_players_4, R.id.menu_set_players_5, R.id.menu_set_players_6};
    private boolean mIsAutoInputEnable = true;
    private int mChangedCell1 = -1;
    private int mChangedCell2 = -1;
    private int mDrawMatchCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_scrollview);
        initView();
        initData();
        intListener();
    }


    private void initView() {
        mScrollView = (ScrollView) findViewById(R.id.scv_main_scroll_view);
        mEdtNames = new MyAutoCompleteTextView[AppConstants.MAX_NUMBER_PLAYER];
        mTv = new TextView[AppConstants.MAX_NUMBER_PLAYER];
        mBtnUp = new ImageButton[AppConstants.MAX_NUMBER_PLAYER];
        mBtnDown = new ImageButton[AppConstants.MAX_NUMBER_PLAYER];
        mLlNps = new LinearLayout[AppConstants.MAX_NUMBER_PLAYER];
        for (int i = 0; i < AppConstants.MAX_NUMBER_PLAYER; i++) {
            mEdtNames[i] = (MyAutoCompleteTextView) findViewById(edtIds[i]);
            mTv[i] = (TextView) findViewById(npIds[i]);
            mBtnUp[i] = (ImageButton) findViewById(btnUp[i]);
            mBtnDown[i] = (ImageButton) findViewById(btnDown[i]);
            mLlNps[i] = (LinearLayout) findViewById(llNps[i]);
        }
        mRcvListScore = (RecyclerView) findViewById(R.id.rcv_recycler_view);
        mImgAdd = (ImageView) findViewById(R.id.img_add);
        mTvMatch = (TextView) findViewById(R.id.tv_match);
        mTvMatch.setVisibility(View.INVISIBLE);
    }

    private void updateRowsVisibility() {
        int state;
        for (int i = 0; i < AppConstants.MAX_NUMBER_PLAYER; i++) {
            if (i < mNumberPlayers) {
                state = View.VISIBLE;
            } else {
                state = View.GONE;
            }
            mEdtNames[i].setVisibility(state);
            mLlNps[i].setVisibility(state);
        }
        mAdapter.updateRowsVisibility(mNumberPlayers);
    }

    private void initData() {
        mScores = new int[mNumberPlayers];
        mItems = new LinkedList<>();
        mAdapter = new ScoreAdapter(mItems);
        mDrawMatchCount = 0;
        mRcvListScore.setAdapter(mAdapter);
        mRcvListScore.setLayoutManager(new LinearLayoutManager(this));
        mRcvListScore.setNestedScrollingEnabled(false);
        updateRowsVisibility();
        setDefaultScores();
        updateScoresTextView();
        updateMatchTextView();
    }


    private void intListener() {
        mImgAdd.setOnClickListener(this);
        for (int i = 0; i < AppConstants.MAX_NUMBER_PLAYER; i++) {
            mBtnUp[i].setOnClickListener(this);
            mBtnDown[i].setOnClickListener(this);
        }
        mAdapter.setOnItemLongClickListener(new ScoreAdapter.OnItemLongClickListener() {
            @Override
            public void onItemLongClick() {
                //Todo
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        for (int i = 0; i < mMenuSetPlayers.length; i++) {
            if (item.getItemId() == mMenuSetPlayers[i]) {
                mNumberPlayers = i + 2;
                initData();
                return true;
            }
        }
        switch (item.getItemId()) {
            case R.id.menu_clear:
                initData();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    private void onClickAddNewRow() {
        Item item = new Item();
        ArrayList<Integer> numbers = new ArrayList<>();
        for (int i = 0; i < mNumberPlayers; i++) {
            numbers.add(Integer.parseInt(mTv[i].getText().toString()));
        }
        item.setNumbers(numbers);
        addItem(item);
        mAdapter.notifyDataSetChanged();
        updateMatchTextView();
        resetNumberPickers();
        scrollViewToBottom();
    }

    private void scrollViewToBottom() {
//        mRcvListScore.smoothScrollToPosition(mItems.size() - 1);
        mScrollView.postDelayed(new Runnable() {
            @Override
            public void run() {
                mScrollView.fullScroll(View.FOCUS_DOWN);
            }
        }, 500);
    }

    private void resetNumberPickers() {
        for (int i = 0; i < mNumberPlayers; i++) {
            mTv[i].setText("0");
        }
    }

    private void updateMatchTextView() {
        mTvMatch.setVisibility(View.VISIBLE);
        int matchCount = mDrawMatchCount + Math.max((mItems.size() - 1), 0);
        mTvMatch.setText("Match : " + matchCount);
    }

    private void addItem(Item item) {
        if (mItems.isEmpty()) {
            //add 2 times to create summary row
            mItems.add(item);
            mItems.add(item);
        } else {
            Item sumItem = Item.sum(mItems.getLast(), item);
            mItems.removeLast();
            mItems.add(item);
            mItems.add(sumItem);
        }
    }

    private void updateScoresTextView() {
        for (int i = 0; i < mNumberPlayers; i++) {
            mTv[i].setText("" + mScores[i]);
        }
    }


    @Override
    public void onClick(View v) {
        for (int i = 0; i < mNumberPlayers; i++) {
            if (v.getId() == btnUp[i]) {
                mScores[i]++;
                mTv[i].setText(mScores[i] + "");
                onScoreChange(i);
                return;
            } else if (v.getId() == btnDown[i]) {
                mScores[i]--;
                mTv[i].setText(mScores[i] + "");
                onScoreChange(i);
                return;
            }
        }
        switch (v.getId()) {
            case R.id.img_add:
                if (Utils.getSumOfArray(mScores) != 0) {
                    Toast.makeText(this, "Wrong input", Toast.LENGTH_SHORT).show();
                } else if (Utils.isZeroArray(mScores)) {
                    onClickAddDrawMatch();
//                    Toast.makeText(this, "Draw, don't need input", Toast.LENGTH_SHORT).show();
                } else {
                    onClickAddNewRow();
                    setDefaultScores();
                }
                break;
        }
    }

    private void onClickAddDrawMatch() {
        DialogUtils.showConfirmDialog(this, R.string.confirm_add_a_draw_match, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mDrawMatchCount++;
                updateMatchTextView();
            }
        });
    }


    /**
     * Method to check then input score automatically
     *
     * @param cellIndex
     */
    private void onScoreChange(int cellIndex) {
        if (mNumberPlayers >= 5 || !mIsAutoInputEnable) {
            return;
        }
        int emptyCells = 0;
        if (mNumberPlayers == 2 || mNumberPlayers == 3) {
            emptyCells = 1;
        } else if (mNumberPlayers == 4) {
            emptyCells = 2;
        }
        if (mChangedCell1 == -1) {
            mChangedCell1 = cellIndex;
        } else if (cellIndex != mChangedCell1) {
            mChangedCell2 = cellIndex;
        }
        if (mNumberPlayers == 2) {
            inputScoreAutomatically(cellIndex);
        } else if (mChangedCell1 != -1 && mChangedCell2 != -1) {
            inputScoreAutomatically(mChangedCell1, mChangedCell2, emptyCells);
        }
    }

    private void inputScoreAutomatically(int changedCell) {
        int valueToInput = 0 - mScores[changedCell];
        for (int i = 0; i < mNumberPlayers; i++) {
            if (i != changedCell) {
                mScores[i] = valueToInput;
            }
        }
        updateScoresTextView();
    }

    private void inputScoreAutomatically(int changedCell1, int changedCell2, int emptyCells) {
        int valueToInput = 0 - (mScores[changedCell1] + mScores[changedCell2]) / emptyCells;
        for (int i = 0; i < mNumberPlayers; i++) {
            if (i != changedCell1 && i != changedCell2) {
                mScores[i] = valueToInput;
            }
        }
        updateScoresTextView();
    }

    private void setDefaultScores() {
        for (int i = 0; i < mNumberPlayers; i++) {
            mScores[i] = 0;
        }
        mChangedCell1 = -1;
        mChangedCell2 = -1;
    }

}
