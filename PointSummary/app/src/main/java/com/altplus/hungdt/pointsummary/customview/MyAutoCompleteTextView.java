package com.altplus.hungdt.pointsummary.customview;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.altplus.hungdt.pointsummary.R;
import com.altplus.hungdt.pointsummary.utils.KeyBoardUtils;

/**
 * Created by Nguyen Dang Hung on 1/3/2017.
 */

public class MyAutoCompleteTextView extends AutoCompleteTextView implements AdapterView.OnItemClickListener {
    public MyAutoCompleteTextView(Context context) {
        super(context);
        init();
    }

    public MyAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }


    private void init() {
        String[] names = getResources().getStringArray(R.array.names);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getContext(), R.layout.item_list_names_drop_down, names);
        this.setAdapter(adapter);
        setOnItemClickListener(this);
        clearFocus();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.d("hungdt", "onTouchEvent");
        if (event.getAction() == MotionEvent.ACTION_UP) {
            showDropDown();
        }
        return super.onTouchEvent(event);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        KeyBoardUtils.hideSoftInput(getContext(), this);
    }
}
