package com.altplus.hungdt.pointsummary;

/**
 * Created by MSI on 10/29/2016.
 */

public class Utils {
    public static int getSumOfArray(int[] A) {
        int sum = 0;
        for (int i : A) {
            sum += i;
        }
        return sum;
    }

    public static boolean isZeroArray(int[] A) {
        for (int i : A) {
            if (i != 0) {
                return false;
            }
        }
        return true;
    }
}
