package com.altplus.hungdt.pointsummary;

import java.util.ArrayList;

/**
 * Created by Nguyen Dang Hung on 10/28/2016.
 */
public class Item {
    ArrayList<Integer> numbers = new ArrayList<>();

    public void setNumbers(ArrayList<Integer> number) {
        numbers = number;
    }

    public ArrayList<Integer> getNumbers() {
        return numbers;
    }

    public static Item sum(Item item1, Item item2) {
        Item sumItem = new Item();
        for (int i = 0; i < item1.getNumbers().size(); i++) {
            sumItem.getNumbers().add(item1.getNumbers().get(i) + item2.getNumbers().get(i));
        }
        return sumItem;
    }
}
